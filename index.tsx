import * as React from 'react'
import * as ReactDOM from 'react-dom';

type record = [number, number, string];

const checkRows = (history: record[], size: number) => {
  // Count X's moves for each row
  let XCount = new Array(size).fill(0);
  let OCount = new Array(size).fill(0);
  for (let i = 0; i < history.length; i += 2) {
    const [row] = history[i];
    if (XCount[row]) {
      XCount[row] += 1
    } else {
      XCount[row] = 1
    }
  }
  // O's moves
  for (let i = 1; i < history.length; i += 2) {
    const [row] = history[i];
    if (OCount[row]) {
      OCount[row] += 1
    } else {
      OCount[row] = 1
    }
  }
  return ({ 'X': Math.max(...XCount), 'O': Math.max(...OCount) });
}
const checkCols = (history: record[], size: number) => {
  let XCount = new Array(size).fill(0);
  let OCount = new Array(size).fill(0);
  for (let i = 0; i < history.length; i += 2) {
    const [_, col] = history[i];
    if (XCount[col]) {
      XCount[col] += 1
    } else {
      XCount[col] = 1
    }
  }
  // O's moves
  for (let i = 1; i < history.length; i += 2) {
    const [_, col] = history[i];
    if (OCount[col]) {
      OCount[col] += 1
    } else {
      OCount[col] = 1
    }
  }
  return ({ 'X': Math.max(...XCount), 'O': Math.max(...OCount) });
}

const checkDiagonal = (history: record[]) => {
  const diags = history.filter(h => h[0] === h[1]);
  const scores = { 'X': 0, 'O': 0 };
  for (let i = 0; i < diags.length; i += 1) {
    scores[diags[i][2]] += 1;
  }
  return scores;
}

const checkAntiDiagonal = (history: record[], size: number) => {
  const diags = history.filter(h => h[0] + h[1] === (size - 1));
  const scores = { 'X': 0, 'O': 0 };
  for (let i = 0; i < diags.length; i += 1) {
    scores[diags[i][2]] += 1;
  }
  return scores;
}

const checkWinner = (history: record[], size: number): string => {
  const fns = [checkRows, checkCols, checkDiagonal, checkAntiDiagonal];
  for (let i = 0; i < fns.length; i += 1) {
    const fn = fns[i];
    const score = fn.call(null, history, size);
    if (score['X'] === size) {
      return 'X'
    }
    if (score['O'] === size) {
      return 'O'
    }
  }
  return null;
}
const Grid = ({ size, onClick, history }) => {
  const arr = [];
  for (let i = 0; i < size; i += 1) {
    for (let j = 0; j < size; j += 1) {
      const isChecked = history.find((h: record) => h[0] === i && h[1] === j);
      const isX = isChecked && isChecked[2] === 'X';
      arr.push(
        <div key={i + '-' + j}
          onClick={() => onClick(i, j)}
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            color: isX ? 'blue' : 'tomato',
            fontSize: '2 rem',
            border: '1px solid black', width: '64px', height: '64px'
          }}>
          {isChecked && isX && <i className="fas fa-times fa-4x" />}
          {isChecked && !isX && <i className="far fa-circle fa-4x" />}
        </div>
      )
    }
  }
  return arr;
}
const Game = () => {
  const [size, setSize] = React.useState<number>(3);
  const [sizeInput, setSizeInput] = React.useState<number>(3);
  const [history, setHistory] = React.useState<record[]>([]); // history of moves
  const [isXMove, setXMove] = React.useState<boolean>(true);
  const [result, setResult] = React.useState(null); // winner result
  const initBoard = () => {
    setHistory([]);
    setSize(Number(sizeInput));
    setXMove(true);
    setResult(null);
  }
  const handleInputChange = (e: any) => {
    setSizeInput(e.target.value);
  }
  const isLegalMove = (row: number, col: number): boolean => {
    // find if row and col is already checked
    if (result) {
      return false;
    }
    const isChecked = history.find((h: record) => h[0] === row && h[1] === col);
    return !isChecked;
  }
  const handleCellClick = (row: number, col: number) => {
    if (isLegalMove(row, col)) {
      const newRecord = [row, col, isXMove ? 'X' : 'O'];
      setXMove(!isXMove);
      const newHistory = [...history, newRecord];
      setHistory(newHistory);
      const winner = checkWinner(newHistory, size);
      if (winner) {
        setResult(winner + ' is Winner');
      } else if (newHistory.length === size * size) {
        setResult('Game Draw');
      }
    }
  }
  const gridWidth = 64 * size + size * 2;
  return (
    <>
      <div style={{ display: 'flex', padding: '0 10px' }}>
        <input
          style={{ padding: '10px', border: '1px solid gray', width: '100%' }}
          placeholder="Enter N (for NXN) "
          value={sizeInput} onChange={handleInputChange} />
        <button
          style={{ padding: '10px 20px', textTransform: 'uppercase', color: '#fff', cursor: 'pointer', border: 'none', background: '#000' }}
          onClick={initBoard}>Start</button>
      </div>
      <hr />
      <div style={{ display: 'flex', flexWrap: 'wrap', width: gridWidth + 'px', margin: 'auto' }}>
        <Grid size={size} history={history} onClick={handleCellClick} />
        <br />
        <h3>{result}</h3>
      </div>
    </>
  )
}

ReactDOM.render(<Game />, document.getElementById("root"));